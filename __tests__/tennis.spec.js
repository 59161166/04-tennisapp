//TennisApp
// - getScore(player)//A,B
// - echo() //return 
//0-0  =  "LOVE-LOVE", 
//15-0 =  "Fifteen-LOVE"

class TennisApp {
    constructor() {
        this.scoreA = 0
        this.scoreB = 0
    }
    echo(){
        var scoreText = ["LOVE", "Fifteen", "Thirty","Fourty"];
        if(this.scoreA===4) {
            return "PlayerA WIN"
        }
        if(this.scoreB===4) {
            return "PlayerB WIN"
        }
        return scoreText[this.scoreA]+" - "+scoreText[this.scoreB]
    }
    getScore(player){
        if (player === 'A') this.scoreA++
        if (player === 'B') this.scoreB++
    }
}

describe('TennisApp', () => {
    it('should return "LOVE - LOVE" when call echo() at game start', () => {
        //Arrange
        const app = new TennisApp()
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('LOVE - LOVE')
    })
    it('should echo "Fifteen - LOVE" when playerA get first score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Fifteen - LOVE')
    })
    it('should echo "Thirty - LOVE" when playerA get seccond score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Thirty - LOVE')
    })
    it('should echo "Thirty - Thirty" when playerB get seccond score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        app.getScore('B')
        app.getScore('B')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('Thirty - Thirty')
    })
    it('should echo "PlayerB WIN" when playerB get forth score', () => {
        //Arrange
        const app = new TennisApp()
        app.getScore('A')
        app.getScore('A')
        app.getScore('B')
        app.getScore('B')
        app.getScore('B')
        app.getScore('B')
        //Act
        let result = app.echo()
        //Assert
        expect(result).toBe('PlayerB WIN')
    })
})